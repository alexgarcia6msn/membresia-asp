use master
drop database membresia
create database membresia;
use membresia;



create table nivel_membresia(
id int identity(1,1) PRIMARY KEY,
nombre varchar(60),
descripcion varchar(max),
procentaje_descuento float,
visitas_necesarias int not null default 0
)

insert into nivel_membresia values ('Bronce', 'Primer nivel', '5', 0)
insert into nivel_membresia values ('Plata', 'Segundo nivel', '8', 5)
insert into nivel_membresia values ('Oro', 'Tercer nivel', '10', 10)


create table miembro(
id int identity (1,1) PRIMARY KEY,
nombre varchar(max) not null,
apaterno varchar(max) not null,
amaterno varchar(max) not null,
genero varchar(20) not null,
fecha_nacimiento datetime not null,


fecha_inscripcion datetime default GETDATE(),
id_nivel_membresia int default 1,
puntos_acumulados float default 0,
visitas_mensuales float default 0,
contraseņa varchar(max) not null,

--datos de contacto--
email varchar(max) not null,
telefono varchar(15),
FOREIGN KEY(id_nivel_membresia) REFERENCES nivel_membresia(id),
)



select * from miembro
select * from nivel_membresia
