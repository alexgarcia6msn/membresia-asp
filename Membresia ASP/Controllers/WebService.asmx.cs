﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Membresia_ASP.Models;


namespace Membresia_ASP.Controllers
{
    /// <summary>
    /// Descripción breve de WebService
    /// </summary>
    //[WebService(Namespace = "http://192.168.1.245/membresias")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class WebService : System.Web.Services.WebService
    {
        membresiaEntities db = new membresiaEntities();
        miembro miembro;

        [WebMethod]
        public Respuesta GetMembershipInfo(int id)
        {
            try
            {
                miembro = db.miembro.Find(id);
                return new Respuesta(miembro);
            }
            catch (Exception)
            {
                return new Respuesta();
            }
        }

        [WebMethod]
        public string AgregarPuntosTaquillaVentaOnline(int id, float Pago)
        {
            try
            {
                miembro = db.miembro.Find(id);
                miembro.puntos_acumulados += Pago / 10;
                miembro.visitas_mensuales += 1;

                if (miembro.visitas_mensuales == miembro.nivel_membresia.visitas_necesarias)
                {
                    miembro.id_nivel_membresia += 1;
                }

                db.SaveChanges();
                return "OK";
            }
            catch (Exception)
            {
                return "ERROR";
            }
        }

        [WebMethod]
        public string AgregarPuntosOtrosServicios(int id, float Pago)
        {
            try
            {
                miembro = db.miembro.Find(id);
                miembro.puntos_acumulados += Pago / 10;
                db.SaveChanges();
                return "OK";
            }
            catch (Exception)
            {
                return "ERROR";
            }
        }

        /// <summary>
        /// Método que verifica si tienes suficientes puntos para pagar únicamente con puntos
        /// </summary>
        /// <param name="id">ID o número de la membresía a verificar</param>
        /// <param name="Pago">Monto que se desea pagar con el puntaje</param>
        /// <returns>Retorna un TRUE en caso de que la membresía suficientes puntos para realizar 
        /// el pago completamente, retorna un FALSE en caso contrario o en caso que la membresía no sea
        /// encontrada
        /// </returns>
        [WebMethod]
        public bool VerificarSiTienesSuficientesPuntosParaPagar(int id, double Pago)
        {
            try
            {
                miembro = db.miembro.Find(id);
                if (Pago <= miembro.puntos_acumulados)
                {
                    return true;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        [WebMethod]
        public double DescontarPuntos(int id, double Pago)
        {
            try
            {
                double retorno;
                miembro = db.miembro.Find(id);

                if (Pago <= miembro.puntos_acumulados)
                {
                    miembro.puntos_acumulados -= Pago;
                    db.SaveChanges();
                    return 1;
                }
                else
                {
                    retorno = Pago - miembro.puntos_acumulados;
                    miembro.puntos_acumulados = 0;
                    db.SaveChanges();
                    return retorno;
                }
            }
            catch (Exception)
            {
                return -1;
            }
        }

        [WebMethod]
        public double CalcularDescuento(int id, float pago)
        {
            try
            {
                miembro = db.miembro.Find(id);
                var retorno = (pago * (miembro.nivel_membresia.procentaje_descuento / 100));
                return retorno;
            }
            catch (Exception)
            {
                return -1;
            }
        }

        [WebMethod]
        public string VerificarLogin(string email, string contraseña)
        {
            try
            {
                var miembro = db.miembro.Where(e => e.email == email).First();
                if (miembro.contraseña == contraseña)
                    return "OK";
            }
            catch (Exception) { }
            return "ERROR";
        }
    }

    public class Respuesta {
        public int id { get; set; }
        public string nombre_completo { get; set; }
        public DateTime fecha_nacimiento { get; set; }
        public Nullable <int> id_nivel_membresia { get; set; }
        public double puntos_acumulados { get; set; }

        public string nombre_tipo_membresia { get; set; }
        public Nullable<double> porcentaje_descuento { get; set; }

        public string StatusMessage;
        public Respuesta() {
            StatusMessage = "ERROR";
        }

        public Respuesta(miembro m) {
            id = m.id;
            nombre_completo = m.nombre + " " + m.apaterno + " "+ m.amaterno;
            fecha_nacimiento = m.fecha_nacimiento;
            id_nivel_membresia = m.id_nivel_membresia;
            puntos_acumulados = m.puntos_acumulados;
            nombre_tipo_membresia = m.nivel_membresia.nombre;
            porcentaje_descuento = m.nivel_membresia.procentaje_descuento;

            StatusMessage = "OK";
        }
    }
}
