﻿using System;
using Membresia_ASP.Models;
using System.Web.Mvc;

namespace Membresia_ASP.Controllers
{
    public class AccountController : Controller
    {
        membresiaEntities db = new membresiaEntities();

        private ActionResult GlobalValidation()
        {
            if (Session["id_membresia"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            var miembro = db.miembro.Find(Session["id_membresia"]);

            ViewBag.id_nivel_membresia_pic = 
                "/Content/pictures/" +
                miembro.nivel_membresia.nombre +
                "-Package-Graphics-Design.png";

            return View(miembro);
        }

        public ActionResult Index()
        {
            return GlobalValidation();
        }

        public ActionResult Code()
        {
            return GlobalValidation();
        }

        public ActionResult Edit()
        {
            return GlobalValidation();
        }

        [HttpPost]
        public ActionResult Edit(miembro miembro)
        {
            miembro.id = Convert.ToInt32(Session["id_membresia"]);
            if (ModelState.IsValid)
            {
                try
                {
                    using (var db = new membresiaEntities())
                    {
                        db.miembro.Add(miembro);
                        db.SaveChanges();
                    }
                    return View("Index");
                }
                catch (Exception){}
            }
            return View();
        }

        public ActionResult Subscriptions()
        {
            return GlobalValidation();
        }

        public ActionResult BenefitsMember()
        {
            return GlobalValidation();
        }
    }

}