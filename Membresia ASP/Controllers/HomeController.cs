﻿using Membresia_ASP.Models;
using Membresia_ASP.BancoService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Membresia_ASP.Controllers
{
    public class HomeController : Controller
    {
        membresiaEntities db = new membresiaEntities();

        public ActionResult Index() {
            ViewBag.backgroundColor = "white";
            Session["id_membresia"] = null;
            return View();
        }

        public ActionResult Benefits()
        {
            ViewBag.backgroundColor = "black";
            Session["id_membresia"] = null;
            return View();
        }

        public string VerificarLogin(FormCollection frmLogin)
        {
            try
            {
                var miembro = db.miembro.Find(Convert.ToInt32(frmLogin["idLogin"].ToString()));
                if (miembro.contraseña == frmLogin["passwordLogin"].ToString())
                {
                    Session["id_membresia"] = miembro.id;
                    return "OK";
                }
            }
            catch (Exception) { }
            return "Favor de verificar sus datos";
        }


        /// <summary>
        /// Permite acceder por primera vez a Home/Signup
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Signup() {
            return View();
        }

        /// <summary>
        /// Redirección a Home/Signup sin perder la información registrada anteriormente
        /// </summary>
        /// <param name="miembro"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Signup(miembro miembro) {
            return View(miembro);
        }

        public ActionResult Payment()
        {
            return View("Index");
        }

        /// <summary>
        /// Verifica si se puede pasar a Home/Payment tras completar el formulario de Home/Signup
        /// </summary>
        [HttpPost]
        public ActionResult Payment(miembro miembro)
        {
            if (ModelState.IsValid)
            {
                return View("Payment", miembro);
            }
            return View("Signup", miembro);
        }

        public ActionResult CheckTransaction()
        {
            return View("Index");
        }

        /// <summary>
        /// Verifica la información bancaria con la que se desea realizar el pago
        /// </summary>
        /// <param name="FormularioBancario"></param>
        /// <returns>
        /// "OK" cuando la transacción se realizó correctamente
        /// En caso contrario retorna una cadena con la especificación del error
        /// </returns>
        [HttpPost]
        public string CheckTransaction(FormCollection FormularioBancario)
        {
            DatosBancarios Pagador = new DatosBancarios();
            Pagador.NombreCompleto = FormularioBancario["NombreCompleto"].ToString();
            Pagador.Sucuenta = FormularioBancario["Sucuenta"].ToString();
            Pagador.pin = FormularioBancario["pin"].ToString();
            Pagador.fechaExp = FormularioBancario["fechaExp"].ToString();
            try
            {
                WSTransferenciaSoapClient BancoService = new WSTransferenciaSoapClient();
                if (!TryValidateModel(Pagador))
                    return "Favor de Verificar los datos bancarios introducidos";

                bool resultado = BancoService.Pago(Pagador.Sucuenta, "6969696969696969",  49,
                    Convert.ToInt32(Pagador.pin), Pagador.NombreCompleto, Pagador.fechaExp, "Cine-membresia");

                if (!resultado)
                    return "Hubo un error al intentar realizar el pago";


                return "OK";
            }
            catch (Exception)
            {
                return "Hubo un error al intentar conectarse con el servicio bancario";
            }
        }

        public ActionResult FinalizarRegistro()
        {
            return View("Index");
        }

        /// <summary>
        /// Genera el Registro de la membresía
        /// </summary>
        /// <param name="miembro"></param>
        /// <returns></returns>
        [HttpPost]
        public int FinalizarRegistro(FormCollection FormularioMembresia)
        {
            miembro miembro = new miembro();

            miembro.nombre = FormularioMembresia["nombre"].ToString();
            miembro.amaterno = FormularioMembresia["amaterno"].ToString();
            miembro.apaterno = FormularioMembresia["apaterno"].ToString();
            miembro.genero = FormularioMembresia["genero"].ToString();
            miembro.fecha_nacimiento = Convert.ToDateTime(FormularioMembresia["fecha_nacimiento"]);
            miembro.email = FormularioMembresia["email"].ToString();
            miembro.telefono = FormularioMembresia["telefono"].ToString();
            miembro.contraseña = FormularioMembresia["contraseña"].ToString();

            if (!TryValidateModel(miembro))
            {
                return -1;
            }

            try
            {
                using (var db = new membresiaEntities())
                {
                    db.miembro.Add(miembro);
                    db.SaveChanges();
                    Session["id_membresia"] = miembro.id;
                    return miembro.id;
                }
            }
            catch (Exception)
            {
                return -1;
            }
        }
    }
}