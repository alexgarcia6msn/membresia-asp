﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Membresia_ASP.Models
{
    public class DatosBancarios
    {
        [Required]
        [RegularExpression(@"^[0-9]{16}$", 
            ErrorMessage = "El no. de cuenta se constituye de 16 numeros.")]
        public string Sucuenta;
        [Required]
        public string NombreCompleto, fechaExp;
        [Required]
        [RegularExpression(@"^[0-9]{3}$",
            ErrorMessage = "El PIN debe constar de 3 valores numéricos.")]
        public string pin;
    }
}