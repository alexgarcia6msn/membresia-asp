﻿var data;
var id;
$('.datepicker').datepicker({
    format: 'mm-yyyy',
    startDate: '-3d'
});
$("#btnAtras").click(function () {
    $("#FormularioMembresia").submit();
});
                                                                       
$("#CheckTransaction").click(function () {                             
    data = $("#FormularioBancario").serialize();
    $.post("/Home/CheckTransaction", data, function (respuesta) {
        if (respuesta !== "OK") {
            alert(respuesta);
            return;
        }
        data = $("#FormularioMembresia").serialize();
        $.post("/Home/FinalizarRegistro", data, function (respuesta) {
            id = respuesta;
            $("#colId").html("ID:" + id);
            $("#colNombre").html("Nombre: " + $("#nombre").val() + " " + $("#apaterno").val() + " " + $("#amaterno").val());
            $('#modal-info').modal();
        });
	});
});