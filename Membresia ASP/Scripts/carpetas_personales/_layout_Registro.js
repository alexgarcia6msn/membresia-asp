﻿var startDate = new Date('1900-01-01'),
    endDate = new Date();
$('.datepicker').datepicker({
    format: 'dd-mm-yyyy',
    startDate: startDate,
    endDate: endDate,
    setDate: startDate
});


$('#TerminosCondiciones').click(function () {
    if (this.checked) {
        $("#btnSubmit").removeAttr("disabled");
        $("#btnSubmit").attr('style', 'background-color:gold');
        return;
    }
    $("#btnSubmit").attr("disabled", true);
    $("#btnSubmit").attr('style', 'background-color:white');
});

$("#btnSubmit").click(function () {
    if (this.hasAttribute("disabled")) {
        return;
    }

    if ($("#contrase_a").val() === $("#confirmarContraseña").val()) {
        $("#FormularioRegistro").submit();
        return;
    }
    //Mas adelante podermos poner un modal en lugar del alert
    alert('Las contraseñas deben coincidir');
});